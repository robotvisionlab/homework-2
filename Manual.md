#Main page
This is main page in program. You can typing command in this page.
This program have three command.( Show, edit , read )


#show
1. Do typing show command in command line.
2. You can see all saved document title.


#edit
1. Do typing edit command in command line.
2. Select shelf and typing book , conference or article. (If you wrong name, you will not be able to edit.)
3. Write down the number of the bibtex you want to edit.
4. The arguments for bibtex will appear. Write the arguments you want to edit
5. Change argument
6. You can check changed argument.


#read
1. Do typing read command in command line
2. Insert file name.
3. Reads the file and stores it in shelf.
4. You can check new item in article shelf.

