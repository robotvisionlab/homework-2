#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <io.h>
#define SIZE 100

using namespace std;


class FileProcessing
{
private:
	string entityType;
	vector<string> lines;
public:
	vector<string> dataType;
	vector<string> dataContent;

	vector<string> readFile(string fileName);
	void showHead();
	void showDetail();
	string getEntityType();
	void seperateData();
	void showAll();

};

class Article {

public:
	string author;
	string title;
	string journal;
	int year;
	int number;
	string pages;
	int month;
	string note;
	int volume;

	//function
	void showDetail() {
		cout << "Author : " << author << endl;
		cout << "title : " << title << endl;
		cout << "journal : " << journal << endl;
		cout << "year : " << year << endl;
		cout << "number : " << number << endl;
		cout << "pages : " << pages << endl;
		cout << "month : " << month << endl;
		cout << "note : " << note << endl;
		cout << "volume : " << volume << endl;
	}
	void readInput(string inputType, string inputContent)
	{
		if (inputType == "author")
		{
			author = inputContent;
		}
		if (inputType == "title")
		{
			title = inputContent;
		}
		if (inputType == "journal")
		{
			journal = inputContent;
		}
		if (inputType == "pages")
		{
			pages = inputContent;
		}
		if (inputType == "note")
		{
			note = inputContent;
		}
		if (inputType == "year")
		{
			year = stoi(inputContent);
		}
		if (inputType == "number")
		{
			number = stoi(inputContent);
		}
		if (inputType == "month")
		{
			month = stoi(inputContent);
		}
		if (inputType == "volume")
		{
			volume = stoi(inputContent);
		}
	}


};

class Book {
public:
	string author;
	string title;
	string publisher;
	int year;
	int volume;
	int series;
	string address;
	int edition;
	int month;
	string note;
	string isbn;

	//function
	void showDetail() {
		cout << "Author : " << author << endl;
		cout << "Title : " << title << endl;
		cout << "Publisher : " << publisher << endl;
		cout << "Year : " << year << endl;
		cout << "Volume : " << volume << endl;
		cout << "Series : " << series << endl;
		cout << "Address : " << address << endl;
		cout << "Edition : " << edition << endl;
		cout << "Month : " << month << endl;
		cout << "Note : " << note << endl;
		cout << "Isbn : " << isbn << endl;
	}
	void readInput(string inputType, string inputContent) {
		if (inputType == "author") {
			author = inputContent;
		}
		if (inputType == "title") {
			title = inputContent;
		}
		if (inputType == "publisher") {
			publisher = inputContent;
		}
		if (inputType == "address") {
			address = inputContent;
		}
		if (inputType == "note") {
			note = inputContent;
		}
		if (inputType == "year") {
			year = stoi(inputContent);
		}
		if (inputType == "volume") {
			volume = stoi(inputContent);
		}
		if (inputType == "series") {
			series = stoi(inputContent);
		}
		if (inputType == "edition") {
			edition = stoi(inputContent);
		}
		if (inputType == "month") {
			month = stoi(inputContent);
		}
		if (inputType == "isbn") {
			isbn = inputContent;
		}
	}

};

class Conference {
public:
	string author;
	string title;
	string booktitle;
	int year;
	string editor;
	int volume;
	int series;
	int pages;
	string address;
	int month;
	string organization;
	string note;
	string publisher;

	//function
	void showDetail()
	{
		cout << "Author : " << author << endl;
		cout << "Title : " << title << endl;
		cout << "Booktitle : " << booktitle << endl;
		cout << "Year : " << year << endl;
		cout << "Editor : " << editor << endl;
		cout << "Volume : " << volume << endl;
		cout << "Series : " << series << endl;
		cout << "Pages : " << pages << endl;
		cout << "Address : " << address << endl;
		cout << "Month : " << month << endl;
		cout << "Organization : " << organization << endl;
		cout << "Note : " << note << endl;
		cout << "Publisher : " << publisher << endl;

	}
	void readInput(string inputType, string inputContent) {
		if (inputType == "author") {
			author = inputContent;
		}
		if (inputType == "title") {
			title = inputContent;
		}
		if (inputType == "booktitle") {
			booktitle = inputContent;
		}
		if (inputType == "editor") {
			editor = inputContent;
		}
		if (inputType == "address") {
			address = inputContent;
		}
		if (inputType == "organization") {
			organization = inputContent;
		}
		if (inputType == "note") {
			note = inputContent;
		}
		if (inputType == "publisher") {
			publisher = inputContent;
		}
		if (inputType == "year") {
			year = stoi(inputContent);
		}
		if (inputType == "volume") {
			volume = stoi(inputContent);
		}
		if (inputType == "series") {
			series = stoi(inputContent);
		}
		if (inputType == "pages") {
			pages = stoi(inputContent);
		}
		if (inputType == "month") {
			month = stoi(inputContent);
		}
	}


};


class Entity {

public:
	vector<Article> article_shelf;
	vector<Book> book_shelf;
	vector<Conference> conference_shelf;

	//add function
	void addEntity(Article input)
	{
		article_shelf.push_back(input);
	}
	void addEntity(Book input)
	{
		book_shelf.push_back(input);
	}
	void addEntity(Conference input)
	{
		conference_shelf.push_back(input);
	}

	//show All content
	void showAllEntity()
	{
		cout << "------------------Article shelf------------------" << endl;
		if (article_shelf.size() == 0) {
			cout << "Do not exist" << endl;
		}
		else {
			for (int i = 0; i < article_shelf.size(); i++)
			{
				cout << i + 1 << ". ";
				cout << article_shelf[i].title << endl;
			}
		}
		cout << "------------------Book shelf---------------------" << endl;
		if (book_shelf.size() == 0) {
			cout << "Do not exist" << endl;
		}
		else {
			for (int i = 0; i < book_shelf.size(); i++)
			{
				cout << i + 1 << ". ";
				cout << book_shelf[i].title << endl;
			}
		}
		cout << "------------------Conference shelf---------------" << endl;
		if (book_shelf.size() == 0) {
			cout << "Do not exist" << endl;
		}
		else {
			for (int i = 0; i < conference_shelf.size(); i++)
			{
				cout << i + 1 << ". ";
				cout << conference_shelf[i].title << endl;
			}
		}
	}

	bool deleteEntity(string content, int num)
	{
		num -= 1;

		if (content == "article")
		{
			if (num < 0 || num >= article_shelf.size())
			{
				cout << "No data Exit" << endl;
				return false;
			}

			int index = 0;
			for (vector<Article>::iterator it = article_shelf.begin(); it != article_shelf.end(); index++)
			{
				if (index == num)
				{
					it = article_shelf.erase(it); //不能?成arr.erase(it);
				}
				else
				{
					++it;
				}
			}

		}
		if (content == "book")
		{
			if (num < 0 || num >= book_shelf.size())
			{
				cout << "No data Exit" << endl;
				return false;
			}

			int index = 0;
			for (vector<Book>::iterator it = book_shelf.begin(); it != book_shelf.end(); index++)
			{
				if (index == num)
				{
					it = book_shelf.erase(it); //不能?成arr.erase(it);
				}
				else
				{
					++it;
				}
			}
		}
		if (content == "conference")
		{
			if (num < 0 || num >= conference_shelf.size())
			{
				cout << "No data Exit" << endl;
				return false;
			}

			int index = 0;
			for (vector<Conference>::iterator it = conference_shelf.begin(); it != conference_shelf.end(); index++)
			{
				if (index == num)
				{
					it = conference_shelf.erase(it); //不能?成arr.erase(it);
				}
				else
				{
					++it;
				}
			}
		}
		return true;
	}
	bool editEntity(string content, int num) {
		string index;
		if (content == "article") {
			showDetail(article_shelf[num - 1]);
			cout << "What do you want to change(small letter) : ";
			cin >> index;
			if (index == "author") {
				cout << "change author :";
				cin >> article_shelf[num - 1].author;
			}
			if (index == "title") {
				cout << "change title :";
				cin >> article_shelf[num - 1].title;
			}
			if (index == "journal") {
				cout << "change journal :";
				cin >> article_shelf[num - 1].journal;
			}
			if (index == "year") {
				cout << "change year :";
				cin >> article_shelf[num - 1].year;
			}
			if (index == "number") {
				cout << "change number :";
				cin >> article_shelf[num - 1].number;
			}
			if (index == "pages") {
				cout << "change pages :";
				cin >> article_shelf[num - 1].pages;
			}
			if (index == "month") {
				cout << "change month :";
				cin >> article_shelf[num - 1].month;
			}
			if (index == "note") {
				cout << "change note :";
				cin >> article_shelf[num - 1].note;
			}
			if (index == "volume") {
				cout << "change volume :";
				cin >> article_shelf[num - 1].volume;
			}
			else {
				cout << "Do not exist" << endl;
			}
			cout << "---------------------------------" << endl;
			cout << "After editting" << endl;
			showDetail(article_shelf[num - 1]);

			return true;

		}
		else if (content == "book") {
			showDetail(book_shelf[num - 1]);
			cout << "What do you want to change(small letter) : ";
			cin >> index;
			if (index == "author") {
				cout << "change author :";
				cin >> book_shelf[num - 1].author;
			}
			if (index == "title") {
				cout << "change title :";
				cin >> book_shelf[num - 1].title;
			}
			if (index == "publisher") {
				cout << "change publisher :";
				cin >> book_shelf[num - 1].publisher;
			}
			if (index == "year") {
				cout << "change year :";
				cin >> book_shelf[num - 1].year;
			}
			if (index == "volume") {
				cout << "change volume :";
				cin >> book_shelf[num - 1].volume;
			}
			if (index == "series") {
				cout << "change series :";
				cin >> book_shelf[num - 1].series;
			}
			if (index == "address") {
				cout << "change address :";
				cin >> book_shelf[num - 1].address;
			}
			if (index == "edition") {
				cout << "change edition :";
				cin >> book_shelf[num - 1].edition;
			}
			if (index == "month") {
				cout << "change month :";
				cin >> book_shelf[num - 1].month;
			}
			if (index == "note") {
				cout << "change note :";
				cin >> book_shelf[num - 1].note;
			}
			if (index == "isbn") {
				cout << "change isbn :";
				cin >> book_shelf[num - 1].isbn;
			}
			else {
				cout << "Do not exist" << endl;
			}
			cout << "Edit after" << endl;
			showDetail(book_shelf[num - 1]);
			return true;
		}
		else if (content == "conference") {
			showDetail(conference_shelf[num - 1]);
			cout << "What do you want to change(small letter) : ";
			cin >> index;
			if (index == "author") {
				cout << "change author :";
				cin >> conference_shelf[num - 1].author;
			}
			if (index == "title") {
				cout << "change title :";
				cin >> conference_shelf[num - 1].author;
			}
			if (index == "booktitle") {
				cout << "change booktitle :";
				cin >> conference_shelf[num - 1].booktitle;
			}
			if (index == "year") {
				cout << "change year :";
				cin >> conference_shelf[num - 1].author;
			}
			if (index == "editor") {
				cout << "change editor :";
				cin >> conference_shelf[num - 1].editor;
			}
			if (index == "volume") {
				cout << "change volume :";
				cin >> conference_shelf[num - 1].volume;
			}
			if (index == "series") {
				cout << "change series :";
				cin >> conference_shelf[num - 1].series;
			}
			if (index == "pages") {
				cout << "change pages :";
				cin >> conference_shelf[num - 1].pages;
			}
			if (index == "address") {
				cout << "change address :";
				cin >> conference_shelf[num - 1].address;
			}
			if (index == "month") {
				cout << "change month :";
				cin >> conference_shelf[num - 1].month;
			}
			if (index == "organization") {
				cout << "change organization :";
				cin >> conference_shelf[num - 1].organization;
			}
			if (index == "note") {
				cout << "change note :";
				cin >> conference_shelf[num - 1].note;
			}
			if (index == "publisher") {
				cout << "change publisher :";
				cin >> conference_shelf[num - 1].publisher;
			}
			else {
				cout << "Do not exist" << endl;
			}
			cout << "Edit after" << endl;
			showDetail(conference_shelf[num - 1]);
			return true;
		}
		else
		{
			return false;
		}
	}
	void showDetail(Article article) {
		cout << "Author : " << article.author << endl;
		cout << "title : " << article.title << endl;
		cout << "journal : " << article.journal << endl;
		cout << "year : " << article.year << endl;
		cout << "number : " << article.number << endl;
		cout << "pages : " << article.pages << endl;
		cout << "month : " << article.month << endl;
		cout << "note : " << article.note << endl;
		cout << "volume : " << article.volume << endl;
	}
	void showDetail(Book book) {
		cout << "Author : " << book.author << endl;
		cout << "Title : " << book.title << endl;
		cout << "Publisher : " << book.publisher << endl;
		cout << "Year : " << book.year << endl;
		cout << "Volume : " << book.volume << endl;
		cout << "Series : " << book.series << endl;
		cout << "Address : " << book.address << endl;
		cout << "Edition : " << book.edition << endl;
		cout << "Month : " << book.month << endl;
		cout << "Note : " << book.note << endl;
		cout << "Isbn : " << book.isbn << endl;
	}
	void showDetail(Conference conference) {
		cout << "Author : " << conference.author << endl;
		cout << "Title : " << conference.title << endl;
		cout << "Booktitle : " << conference.booktitle << endl;
		cout << "Year : " << conference.year << endl;
		cout << "Editor : " << conference.editor << endl;
		cout << "Volume : " << conference.volume << endl;
		cout << "Series : " << conference.series << endl;
		cout << "Pages : " << conference.pages << endl;
		cout << "Address : " << conference.address << endl;
		cout << "Month : " << conference.month << endl;
		cout << "Organization : " << conference.organization << endl;
		cout << "Note : " << conference.note << endl;
		cout << "Publisher : " << conference.publisher << endl;
	}


};

Entity putInputIntoShelf(string fileName, Entity entities);

int getIntInput()
{
	while (true)
	{
		string str;
		cin >> str;
		
		bool isNumber = true;

		for (int index_str = 0; index_str < str.size(); index_str++)
		{
			if (str[index_str] >= '0' && str[index_str] <= '9') //in range 0 to 9
			{
				//std::cout << "in range" << endl;
			}
			else
			{
				isNumber = false;
				break;
			}
		}

		if (isNumber == true)
		{
			return stoi(str);
		}

		std::cout << "Your input is not number. \nPlease enter an integer number again." << endl;
	}
}

bool checkIndex(string name, int num, Entity entities) 
{

	if (name == "book") {
		if (entities.book_shelf.size() == 0) {
			return false;
		}
		else if (num > entities.book_shelf.size() || num <= 0) {
			return false;
		}
		else {
			return true;
		}

	}
	if (name == "article") 
	{
		if (entities.article_shelf.size() == 0) {
			return false;
		}
		else if (num > entities.article_shelf.size() || num <= 0) {
			return false;
		}
		else {
			return true;
		}
	}
	if (name == "conference") 
	{
		if (entities.conference_shelf.size() == 0) {
			return false;
		}
		else if (num > entities.conference_shelf.size() || num <= 0) {
			return false;
		}
		else {
			return true;
		}
	}
	else
	{
		return false;
	}
}


int main()
{

	Entity entities;

	
	//You can unlock this part if you are tired to enter too many datas.
	entities = putInputIntoShelf("a.bib", entities);
	entities = putInputIntoShelf("b.bib", entities);
	entities = putInputIntoShelf("c.bib", entities);
	entities = putInputIntoShelf("d.bib", entities);
	entities = putInputIntoShelf("e.bib", entities);
	entities = putInputIntoShelf("f.bib", entities);
	
	cout << "------------------------------------------------------------" << endl;


	while (true)
	{
		cout << "What Do you want to do?" << endl;
		string cmd;
		string data;
		cin >> cmd;
		cout << "*******************************************************" << endl;

		if (cmd == "read")
		{
			cout << "file name: ";
			cin >> data;
			entities = putInputIntoShelf(data, entities);
		}

		else if (cmd == "show")
		{
			entities.showAllEntity();
		}

		else if (cmd == "delete")
		{
			int num;
			string shelf;
			cout << "Select the shelf :";
			cin >> shelf;
			cout << "Select number : ";
			num = getIntInput();
			if (checkIndex(shelf, num, entities))
			{
				entities.deleteEntity(shelf, num);
				cout << "You deleted the data!" << endl;
			}
			else
			{
				cout << "Do not exist" << endl;
			}
		}

		else if (cmd == "edit")
		{
			int num;
			string shelf;
			cout << "Select the shelf :";
			cin >> shelf;
			if (checkIndex(shelf, 1, entities))
			{
				cout << "Enter the index of data which you want to edit!" << endl;
				cout << "Select number : ";
				//cin >> num;
				num = getIntInput();
				if (checkIndex(shelf, num, entities))
				{
					entities.editEntity(shelf, num);
					
				}
				else
				{
					cout << "Do not exist" << endl;

				}
			}
			else
			{
				cout << "Do not exist" << endl;
				cout << "There are only [article], [book],  or [conference]..." << endl;
			}
			
			
		}
		else
		{
			cout << "Not supported comand... " << endl;
			cout << "You can try [read], [show], [edit], or [delete]..." << endl;
		}
	
		cout << "*******************************************************" << endl;
	}



	cout << endl << endl;
	system("pause");
	return 0;
}

void FileProcessing::showAll()
{
	for (int index = 1; index < dataContent.size() - 1; index++)
	{
		cout << dataType[index] << "\t" << dataContent[index] << endl;
	}
}

void FileProcessing::seperateData()
{
	for (int index = 1; index < lines.size() - 1; index++)
	{
		string originalStr = lines[index];

		int front = originalStr.find(" ");
		int end = originalStr.find("\n");
		string resultStr = originalStr.substr(2, end - 1);
		//cout << resultStr << "||" << endl;

		originalStr = resultStr;
		front = originalStr.find(" ");
		end = originalStr.find(" ");
		resultStr = originalStr.substr(0, end);

		dataType.push_back(resultStr);

	}

	for (int index = 1; index < lines.size() - 1; index++)
	{
		string originalString = lines[index];
		int front = originalString.find('{');
		int end = originalString.find('}');
		string resultStr = originalString.substr(front + 1, end - 1 - front);

		if (front == -1)
		{
			front = originalString.find('=');
			end = originalString.find(',');
			resultStr = originalString.substr(front + 2, end - 2 - front);

		}

		dataContent.push_back(resultStr);
	}
}

string FileProcessing::getEntityType()
{
	string originalStr = lines[0];
	int front = originalStr.find("@");
	int end = originalStr.find("{");
	//cout << "front = " << front << endl;
	//cout << "endl = " << end << endl;
	string resultStr = originalStr.substr(front + 1, end - 1);
	//cout << resultStr << endl;

	return resultStr;
}

void FileProcessing::showHead()
{
	cout << "entityName: ";
	cout << lines[0] << endl;
}

void FileProcessing::showDetail()
{
	for (int index = 0; index < lines.size(); index++)
	{
		cout << lines[index] << endl;
	}
}

vector<string> FileProcessing::readFile(string fileName)
{
	cout << "Read the file: " << fileName << endl;
	char line_char[SIZE];

	fstream fin;
	fin.open(fileName, ios::in);
	while (fin.getline(line_char, sizeof(line_char), '\n')) {
		string line_str(line_char);
		lines.push_back(line_str);
	}

	entityType = getEntityType();
	seperateData();

	return lines;
}

Entity putInputIntoShelf(string fileName, Entity entities)
{

	if ((_access(fileName.c_str(), 0)) == -1)
	{
		cout << "Cannot find file: " << fileName << endl;
		return entities;
	}

	FileProcessing fileProcessing;
	fileProcessing.readFile(fileName);
	//fileProcessing.showAll();
	cout << "type = " << fileProcessing.getEntityType();

	if (fileProcessing.getEntityType() == "article")
	{
		Article newArticle;
		for (int index = 0; index < fileProcessing.dataContent.size(); index++)
		{
			newArticle.readInput(fileProcessing.dataType[index], fileProcessing.dataContent[index]);
		}

		newArticle.showDetail();
		entities.addEntity(newArticle);
		return entities;
	}

	if (fileProcessing.getEntityType() == "book")
	{
		Book newBook;
		for (int index = 0; index < fileProcessing.dataContent.size(); index++)
		{
			newBook.readInput(fileProcessing.dataType[index], fileProcessing.dataContent[index]);
		}

		newBook.showDetail();
		entities.addEntity(newBook);
		return entities;
	}

	if (fileProcessing.getEntityType() == "conference")
	{
		Conference newConference;
		for (int index = 0; index < fileProcessing.dataContent.size(); index++)
		{
			newConference.readInput(fileProcessing.dataType[index], fileProcessing.dataContent[index]);
		}

		newConference.showDetail();
		entities.addEntity(newConference);
		return entities;
	}
}